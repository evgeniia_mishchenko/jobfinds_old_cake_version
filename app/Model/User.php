<?php

//Password hashing:
// 1. add two lines before User class
// 2. pass function beforeSave inside of our class
// 3. add $components and filter() to the end of AppController

App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

class User extends AppModel {

    public function beforeSave($options = array()) {
        if (isset($this->data[$this->alias]['password'])) {
            $passwordHasher = new BlowfishPasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash(
                $this->data[$this->alias]['password']
            );
        }
        return true;
    }

    //Validation
    public $validate = array(
        'first_name' => array(      //2 validation rules for name
            'alphaNumeric' => array(        //letters and numbers only
                'rule' => 'alphaNumeric',
                'required' => true,
                'message' => 'Letters and numbers only'
            ),
            'notEmpty' => array(            //cant be empty
                'rule' => 'notEmpty',
                'message' => 'Please enter your first name'
            )
        ),
        'last_name' => array(
            'alphaNumeric' => array(
                'rule' => 'alphaNumeric',
                'required' => true,
                'message' => 'Letters and numbers only'
            ),
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'message' => 'Please enter your last name'
            )
        ),
        'email' => array(
            'email' => array(
                'rule' => 'email',
                'required' => true,
                'message' => 'Please enter an email address'
            ),
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'message' => 'Please enter an email address'
            )
        ),
        'password' => array(
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'message' => 'Please enter your password'
            )
        ),
        'confirm_password' => array(
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'message' => 'Please confirm your password'
            ),
            'matchPassword' => array (
                'rule' => array('identicalFieldValues', 'password'),
                'message' => 'Your password do not match'
            )
        )
    );

    /*
     * Custom Validation - Match field values
     */
    function identicalFieldValues( $field=array(), $compare_field = null){
        foreach ($field as $key => $value){
            $v1 = $value;
            $v2 = $this->data[$this->name][$compare_field]; //field that we actually compare
            if($v1 !== $v2){
                return FALSE;
            } else {
                continue;
            }
        }
        return TRUE;
    }

}
