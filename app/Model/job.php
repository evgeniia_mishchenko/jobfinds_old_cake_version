<?php
class Job extends AppModel {
    public $name = 'Job';
    public $belongsTo = array('Type', 'Category');  //simple association, job belongs to a category just like it belongs to a type
}