<?php
//we need hashed password and authentication(login and logout)
// (which goes with cakephp - AuthComponent(implemented in Model and sth in AppController))-
//  class responsible for requiring login for certain actions,
// handling user sign-in and sign-out,
// and also authorizing logged in users to the actions they are allowed to reach.
class UsersController extends AppController {
    public $name = 'Users';

    /*
     * Register User
     */
    public function register(){
        if($this->request->is('post')){
            $this->User->create();
            if($this->User->save($this->request->data)){
                //set a msg and redirect
                $this->Session->setFlash(__('You are now registered and may login'), 'default', array('class' => 'notice success'));
                return $this->redirect(array('controller' => 'jobs', 'action' => 'index'));
            }
            $this->Session->setFlash(__('There was a problem creating your account'));
        }
    }
    /*
     * Login User
     */
    public function login(){
        if($this->request->is('post')){
            //Auth($this->auth->login()) return false in cakephp 2.5.6 if without parameter
            if($this->Auth->login($this->request->data)) {     //all implemented by cakephp, we didnt create Auth object(right conventions!)
                //print_r($this->request->data);    -test
                //if we can login we redirect
                return $this->redirect($this->Auth->redirectUrl());
                //(AppController - $components - Auth - loginRedirect(jobs/index)
                // but we could just write this path
                // but better use what is available in Auth component)
            }
            $this->Session->setflash('Invalid username or password');

        }
    }

    /*
     * Logout User(the component does log out for us)
     */
    public function logout(){
        return $this->redirect($this->Auth->logout());
    }


}