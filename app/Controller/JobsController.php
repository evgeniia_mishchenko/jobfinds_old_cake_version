<?php

class JobsController extends AppController {
    public $name = 'Jobs';
    //Default index method
    public function index() {
        //Set query options
        $options = array(
            'order' => array('Job.created' => 'desc'),  //or asc
            'limit' => 10
        );
        //Get job info
        $jobs = $this->Job->find('all', $options);
        //Set title(near favicon)
        $this->set('title_for_layout', 'JobFinds | Welcome');
        //set View
        $this->set('jobs',$jobs);   //1-name of controller object, 2-nd array

        //Set category query option
        $options = array(
            'order' => array('Category.name' => 'desc')
        );
        //Get categories
        $categories = $this->Job->Category->find('all', $options);
        //Set categories
        $this->set('categories',$categories);   //set the view

    }
    public function browse ($category = null){  //null because we wonna jobs/browse/3 - category id
    // but null by default to just browse if no category so we check if there is category
        //init conditions array
        $conditions = array();

        //Check keyword filter
        if ($this->request->is('post')){
            //die('is post');    first test if it submits
            //now check for individual fields(even if not all are selected)
            if(!empty($this->request->data('keywords'))){   //same as $_POST['keywords'] but more sucure
                //die(($this->request->data('keywords')));
                $conditions[] = array('OR' => array(
                    'Job.title LIKE' => "%" . $this->request->data('keywords') . "%" ,   //which means check title and a description for keywords
                    'Job.description LIKE' => "%" . $this->request->data('keywords') . "%"
                ));
            }
            //State filter
            if(!empty($this->request->data('state')) && $this->request->data('state') != 'Select State'){   //same as $_POST['keywords'] but more sucure
                   //die(($this->request->data('keywords')));
                $conditions[] = array(
                    'Job.state LIKE' => "%" . $this->request->data('state') . "%"  //which means check title and a description for keywords
                );
            }
            //Category filter
            if(!empty($this->request->data('category')) && $this->request->data('category') != 'Select Category'){   //same as $_POST['keywords'] but more sucure
                //die(($this->request->data('keywords')));
                $conditions[] = array(
                    'Job.category_id LIKE' => "%" . $this->request->data('category') . "%"  //which means check title and a description for keywords
                );
            }
        }

        //Set category query option
        $options = array(
            'order' => array('Category.name' => 'asc')
        );
        //Get categories
        $categories = $this->Job->Category->find('all', $options);
        //Set categories
        $this->set('categories',$categories);   //set the view



        if($category != null){
            //Match category (add condition to the query)
            $conditions[] = array(
                'Job.category_id LIKE' => "%" . $category . "%"
            );
        }
        //Set query options
        $options = array (
            'order' => array('Job.created' => 'desc'),
            'conditions' => $conditions,
            'limit' => 8
        );

        //Get job info
        $jobs = $this->Job->find('all', $options);
        //Set title
        $this->set('title_for_layout', 'JobFinds | Browse For a Job');
        $this->set('jobs',$jobs);   //set the view
    }

    /*
     * View single job page
     */
    public function view($id) {
        if(!$id){
            throw new NotFoundException(__('Invalid job listing'));     //cakephp syntax for throwing an error
        }
        $job = $this->Job->findById($id);       //single result using its id
        if(!$job){
            throw new NotFoundException(__('Invalid job listing'));
        }
        //Set title
        $this->set('title_for_layout', $job['Job']['title']);
        $this->set('job', $job);
    }

    /*
     * Add job(to display the form and submit form to it)
     */
    public function add(){
        //Set category query option
        $options = array(
            'order' => array('Category.name' => 'asc')
        );
        //Get categories for select field
        $categories = $this->Job->Category->find('list', $options);
        //Set categories
        $this->set('categories',$categories);

        //Get types for select field
        $types = $this->Job->Type->find('list');
        //Set types
        $this->set('types',$types);

        //if the form is submitted
        if ($this->request->is('post')){
            $this->Job->create();   //we dont even need any sql code thanks to cake helpers
            //user_id field in jobs table - comes from whoever is logged in from registration/login system

            //Save logged user id
            $this->request->data['Job']['user_id'] = $this->Auth->user['id'];

            if($this->Job->save($this->request->data)){
                //set a msg and redirect
                $this->Session->setFlash(__('Your job has been listed'), 'default', array('class' => 'notice success'));
                return $this->redirect(array('action' => 'index'));
            }

            $this->Session->setFlash(__('Unable to send your job'));

        }
    }


    /*
     * Edit job
     */
    public function edit($id){
        //Set category query option
        $options = array(
            'order' => array('Category.name' => 'asc')
        );
        //Get categories for select field
        $categories = $this->Job->Category->find('list', $options);
        //Set categories
        $this->set('categories',$categories);

        //Get types for select field
        $types = $this->Job->Type->find('list');
        //Set types
        $this->set('types',$types);

        //Before check if the form is submitted check for id
        if(!$id){
            throw new NotFoundException(__('Invalid job listing'));
        }
        $job = $this->Job->findById($id);       //single result using its id
        if(!$job){
            throw new NotFoundException(__('Invalid job listing'));
        }

        //if the form is submitted
        if ($this->request->is(array('job', 'put'))){
            //die($this->request->is('job', 'put'));
            $this->Job->id = $id;   //we dont even need any sql code thanks to cake helpers
            //user_id field in jobs table - comes from whoever is logged in from registration/login system

            if($this->Job->save($this->request->data)){
                //set a msg and redirect
                $this->Session->setFlash(__('Your job has been updated'), 'default', array('class' => 'notice success'));
                return $this->redirect(array('action' => 'index'));
            }

            $this->Session->setFlash(__('Unable to update your job'), 'default', array('class' => 'notice error'));

        }
        //automatically prefield edit form with data from DB
        if(!$this->request->data){
            $this->request->data = $job; //perfect example of convention over configurayion
        }
    }

    /*
     * Delete a Job
     */
    public function delete ($id){
        if ($this->request->is('get')){
            throw new MethodNotAllwedException();
        }
        if ($this->Job->delete($id)){
            $this->Session->setFlash(__('The job with id: %s has been deleted.', h($id)));
        }
        return $this->redirect(array('action' => 'index'));
    }
}