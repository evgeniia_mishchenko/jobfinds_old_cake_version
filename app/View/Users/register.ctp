<section>
    <div class="col_4"></div>
    <div class="col_4">
        <?php
        //different way of creating forms( with cakephp form helper)
        echo $this->Form->create('User', array('inputDefaults' => array('label' => false, 'class' => 'col_11 column'), 'class' => 'form_block'));
        ?>
        <fieldset>
            <legend><?php echo __('Create Account'); ?></legend>

            <?php
            echo $this->Form->input('first_name', ['placeholder' => 'First Name']);
            echo $this->Form->input('last_name', ['placeholder' => 'Last Name']);
            echo $this->Form->input('email', array(
                'type' => 'email',
                'placeholder' => 'Email'
            ));
            echo $this->Form->input('username', ['placeholder' => 'Username']);
            echo $this->Form->input('password', array(
                'type' => 'password',
                'placeholder' => 'Password'
            ));
            echo $this->Form->input('confirm_password', array(
                'type' => 'password',
                'placeholder' => 'Confirm Password'

            ));
            echo $this->Form->input('role', array(
                'type' => 'select',
                'options' => array('Job Seeker' => 'Job Seeker', 'Employer' => 'Employer' ),
                'empty' => 'Select User Type'
            ));
            echo $this->Form->button('Register', ['type' => 'submit', 'class' => 'col_11 column register_btn']);
            echo $this->Form->end();
            ?>
        </fieldset>
        <br>
    </div>
    <div class="col_4"></div>
</section>
<div class="clearfix"></div>


