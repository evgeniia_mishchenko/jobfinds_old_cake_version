<section>
    <div class="col_4"></div>
    <div class="col_4">
        <?php echo $this->Session->flash('auth'); ?>
        <?php echo $this->Form->create('User', array('inputDefaults' => array('label' => false, 'class' => 'col_11 column'), 'class' => 'form_block')); ?>
        <fieldset>
            <legend><?php echo __('Please enter your username and password'); ?></legend>
                <?php
                echo $this->Form->input('username', ['placeholder' => 'Username']);
                echo $this->Form->input('password', ['placeholder' => 'Password']);
                echo $this->Form->button('Login', ['type' => 'submit', 'class' => 'col_11 column register_btn']);
                echo $this->Form->end();
                ?>
        </fieldset>
        <br>
    </div>
    <div class="col_4"></div>
</section>
<div class="clearfix"></div>