<section>
    <div class="col_4"></div>
    <div class="col_4">
        <?php
        //different way of creating forms( with cakephp form helper)
        echo $this->Form->create('Job', array('inputDefaults' => array('label' => false, 'class' => 'col_11 column'), 'class' => 'form_block'));
        ?>
        <fieldset>
            <legend><?php echo __('Edit Job Listing'); ?></legend>

            <?php
            echo $this->Form->input('title');
            echo $this->Form->input('company_name');
            echo $this->Form->input('category_id', array(     //select field
                'type' => 'select',
                'options' => $categories,       //go from DB, define in controller before if post
                'empty' => 'Select Category'
            ));
            echo $this->Form->input('type_id', array(
                'type' => 'select',
                'options' => $types,
                'empty' => 'Select Type'
            ));
            echo $this->Form->textarea('description', ['class' => 'col_11 column']);
            echo $this->Form->input('city');
            echo $this->Form->input('state');
            echo $this->Form->input('contact_email');
            echo $this->Form->button('Edit Job', ['type' => 'submit', 'class' => 'col_11 column add_btn']);
            echo $this->Form->end();
            ?>
        </fieldset>
        <br>
    </div>
    <div class="col_4"></div>
</section>
<div class="clearfix"></div>


