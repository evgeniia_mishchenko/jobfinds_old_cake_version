<?php echo $this->element('search'); ?>

<section>
    <div class="col_2"></div>
    <div class="col_8">
        <table class="listings" cellspacing="0" cellpadding="0">
            <thead>
            <tr><th colspan="3"><h4><b>Latest Job Listing</b></h4></th></tr>
            </thead>
            <tbody>
            <?php echo $this->Session->flash(); ?>
            <!-- Success -->


            <?php foreach($jobs as $job) : ?>
                <tr id="type">
                    <td><div class="center job_type" style="background:<?php echo $job['Type']['color']; ?>"><?php echo $job['Type']['name']; ?></div></td> <!--here we need association of jobs with types(very easy)-->
                    <td>
                        <div class="description">
                            <h5><?php echo $job['Job']['title']; ?> (<?php echo $job['Job']['city']; ?>, <?php echo $job['Job']['state']; ?>)</h5>
                            <span id="list_date"><?php echo $this->Time->format('F j.s h:i A', $job['Job']['created']); ?> </span>
                            <?php echo $this->Text->truncate($job['Job']['description'], 250, array('ellipsis' => '...', 'exact' => false)); ?>
                            <!-- truncate - обрізувати-->
                            <?php echo $this->Html->link('Read More', array('controller' => 'jobs', 'action' => 'view', $job['Job']['id'])) ?> <!--jobs/views/id-->
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="col_2"></div>
</section>

<div class="clearfix"></div>
