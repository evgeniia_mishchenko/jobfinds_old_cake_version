        <section id="job_title">
            <div class="col_2"></div>
            <div class="col_8">
                <h3><?php echo $job['Job']['title']; ?></h3>
                <ul>
                    <li><strong>Company: </strong><?php echo $job['Job']['company_name']; ?></li>
                    <li><strong>Location: </strong><?php echo $job['Job']['city']; ?>, <?php echo $job['Job']['state']; ?></li>
                    <li><strong>Job Type: </strong><?php echo $job['Type']['name']; ?></li>
                    <li><strong>Date: </strong><?php echo $job['Job']['created']; ?></li>
                </ul>
            </div>
            <div class="col_2"></div>
        </section>
        <section id="job_description">
            <div class="col_2"></div>
            <div class="col_8">
                <h4><?php echo $job['Job']['title']; ?></h4>
                <p><?php echo $job['Job']['description']; ?></p>
                <!--<h4>Benefits</h4>
                <ul>
                    <li>What candidate can get from the position?</li>
                    <li>What candidate can get from the position?</li>
                    <li>What candidate can get from the position?</li>
                </ul>
                <h4>Job Requirements</h4>
                <ol>
                    <li>Detailed requirement for the vacancy.</li>
                    <li>Detailed requirement for the vacancy.</li>
                    <li>Detailed requirement for the vacancy.</li>
                </ol>-->
                <h4>How To Apply</h4>
                <!--<p>How candidate can apply for your job. You can leave your contact, address to receive hard copy application or any detailed guide for application.</p>
                -->
                <p><strong>Contact Email: </strong><a href=""><?php echo $job['Job']['contact_email']; ?></a></p>
                <p><a href="<?php echo $this->webroot; ?>jobs/browse">Back To Jobs</a></p>
                <br><br>
                <?php if($userData['id'] == $job['Job']['user_id'] ) : ?>
                    <?php echo $this->Html->link('Edit', array('action' => 'edit', $job['Job']['id'])); ?> |
                    <?php echo $this->Form->postLink('Delete', array('action' => 'delete', $job['Job']['id']), array('confirm' => 'Are you sure?')); ?>
                <?php endif; ?>
            </div>
            <div class="col_2"></div>
        </section>

        <div class="clearfix"></div>
