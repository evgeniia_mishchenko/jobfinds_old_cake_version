<section id="search-block">
    <div class="col_2"></div>
    <div class="col_8">
        <div id="search_area">
            <h5 class="center"><strong>Join us & Explore thousands of Jobs</strong></h5>
            <div id="advanced_search" class="center">
                <form action="<?php echo $this->webroot; ?>jobs/browse" method="POST" class="horizontal">
                    <div class="col_4">
                        <input name="keywords" type="text" placeholder="Enter Keywords" />
                    </div>
                    <div class="col_3">
                        <select name="state" id="state_select">
                            <option value="0">Select State</option>
                            <option value="Ukraine">Ukraine</option>
                            <option value="2">Option 2</option>
                            <option value="3">Option 3</option>
                        </select>
                    </div>
                    <div class="col_3">
                        <select name="category" id="category_select" name="category">
                            <option>Select Category</option>
                            <?php foreach($categories as $category) : ?>
                                <option value="<?php echo $category['Category']['id']; ?>"><?php echo $category['Category']['name']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col_2">
                        <button id="search_btn" type="submit">Search</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col_2"></div>
</section>