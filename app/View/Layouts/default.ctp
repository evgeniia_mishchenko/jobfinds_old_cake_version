<!DOCTYPE html>
<html>
<head>
	<?= $this->Html->charset() ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>
		<?= $this->fetch('title') ?>
	</title>
	<?= $this->Html->meta('icon') ?>        <!--for favicon-->

	<?= $this->Html->css('kickstart') ?>    <!--Html - include; fetch - insert-->
    <?= $this->Html->css('custom') ?>
    <?= $this->Html->css('style') ?>

    <?= $this->Html->script('jquery') ?>
	<?= $this->Html->script('kickstart') ?>

	<?= $this->fetch('meta') ?>
	<?= $this->fetch('css') ?>
	<?= $this->fetch('script') ?>
</head>
<body>

<div id="container" class="grid">
	<header>
		<div class="col_2"></div>
		<div class="col_2" style="margin:0;">
			<p class="hide-phone" style="font-size:18px;"><a href="<?php echo $this->webroot; ?>"><strong>Job<span style="color:#e6b706;">Finds</span></strong></a></p>
		</div>
		<div class="col_6" style="margin:0;">
			<ul class="menu right">
				<li <?php echo ($this->here == '/jobfinds/' || $this->here == '/jobfinds/jobs')? 'class="current"' : '' ?>><a href="<?php echo $this->webroot; ?>">Home</a></li>
				<li <?php echo ($this->here == '/jobfinds/jobs/browse')? 'class="current"' : '' ?>><a href="<?php echo $this->webroot; ?>jobs/browse">Browse Jobs</a></li>
				<li <?php echo ($this->here == '/jobfinds/jobs/add')? 'class="current"' : '' ?>>
					<form action="<?php echo $this->webroot; ?>jobs/add">
						<button class="post_btn">Post a Job</button>
					</form>
				</li>
                <li>
                    <?php if(AuthComponent::user('id')) : ?>
                        <h6>Welcome <strong><?php echo $userData['username']; ?></strong></h6>
                        <a href="<?php echo $this->webroot; ?>users/logout">Logout</a>
                    <?php endif; ?>
                </li>
				<li <?php echo ($this->here == '/jobfinds/users/login')? 'class="current"' : '' ?>><a href="<?php echo $this->webroot; ?>users/login"><i class="fa fa-sign-in"></i>  Login</a></li>
                <li <?php echo ($this->here == '/jobfinds/users/register')? 'class="current"' : '' ?>><a href="<?php echo $this->webroot; ?>users/register"><i class="fa fa-key"></i>  Register</a></li>
			</ul>
		</div>
		<div class="col_2"></div>
	</header>
	<div id="content">
		<?php echo $this->fetch('content'); ?>
	</div> <!-- end main content -->

    <footer>
		<p class="center">Copyright &copy | 2015, JobFinds</p>
	</footer>
</div> <!-- End Grid -->

</body>
</html>
